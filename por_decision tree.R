#Clear environment
rm(list =ls())

#Load the rpart, rattle plot and RColorBrewer package
library(rpart)
library(rattle)
library(rpart.plot)
library(RColorBrewer)

#Set Working Directory
setwd("C:\\Desktop\\Predictive Analytics\\Using Data Mining to Predict Secondary School Student Performance\\Por_Decision Tree_Tang Shi-Ming Royston")

#Set random seed. Don't remove this line.
set.seed(1)

##Loading the Data
#Load the data and store in variable Por
Por <- read.csv("Por.csv", stringsAsFactors=F)

#####Splitting the Data
#Split the data into train(65%) and test(35%) datasets
index_train <-sample(nrow(Por), 0.65*nrow(Por), replace=F)
train <-Por[index_train,]
test <- Por[-index_train,]

##Building a Decision Tree
#Build a tree mode (extra educational school support): tree
tree <- rpart(G1 ~.,
              data = train,
              )

tree <- rpart(G2 ~.,
              data = train,
              )

tree <- rpart(G3 ~.,
              data = train,
              )

#Output of the tree model
summary(tree)

#Basic plot
plot(tree, uniform = T)
par(mar=c(1,1,1,1))
text(tree)

#Fanciest plot using rattle library
fancyRpartPlot(tree)

##Making Predictions using a Pruned Tree Model
#Make predictions using pruned tree model & test data and store in pred
pred <- predict(tree,
                newdata = test,
                )

source("util.r")
test_rmse <- RMSE(pred,test$G1)
test_rmse <- RMSE(pred,test$G2)
test_rmse <- RMSE(pred,test$G3)

pred <- predict(tree,
                newdata = train,
)

source("util.r")
train_rmse <- RMSE(pred,train$G1)
train_rmse <- RMSE(pred,train$G2)
train_rmse <- RMSE(pred,train$G3)
