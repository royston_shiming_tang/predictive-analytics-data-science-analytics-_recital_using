# Predictive Analytics (Data Science and Analytics)_Recital_Using Data Mining to Predict Secondary School Student Performance [Paulo Cortez, A. M. Gon�alves Silva, Published 2008]

Analysis secondary school student performance by:

1. The dataset (Math.csv and Por.csv) are related to students� performance in two public schools from Alentejo region of Portugal.

2. Analyze the data by performing data preparation and exploration through visualization and statistical approaches.

3. Build predictive model(s) to predict (estimation and/or classification) the final grade of students. 

Source: Conference paper by Cortex and Silva  published in Proceedings of 5th Future Business Technology Conference (FUBUTEC 2008) for the description of the dataset (http://www3.dsi.uminho.pt/pcortez/student.pdf). The dataset is released to public for research by Cortex, University of Minho.

